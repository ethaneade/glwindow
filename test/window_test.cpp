#include <glwindow/glwindow.hpp>
#include <iostream>
#include <string>
#include <GL/gl.h>

#include <vector>

using namespace glwindow;
using namespace std;

struct Point2D
{
    double x, y;
    Point2D() : x(0), y(0) {}

    Point2D(int x_, int y_) : x(x_), y(y_) {}
};

struct Handler : public EventHandler
{
    std::vector<std::vector<Point2D> > strokes;
    
    std::string all_text;
    
    bool on_button_down(GLWindow& win, int btn, int state, int x, int y)
    {
        if (btn == ButtonEvent::LEFT) {
            strokes.push_back(std::vector<Point2D>(1, Point2D(x,y)));
            return true;
        } else if (btn == ButtonEvent::RIGHT) {
            strokes.clear();
            return true;
        } else if (btn == ButtonEvent::MIDDLE) {
            for (size_t i=0; i<strokes.size(); ++i) {
                for (size_t j=0; j<strokes[i].size(); ++j) {
                    strokes[i][j].x *= 0.5;
                    strokes[i][j].y *= 0.5;
                }
            }
        }
        return false;
    }

    bool on_mouse_move(GLWindow& win, int state, int x, int y)
    {
        if (state & ButtonEvent::LEFT) {
            strokes.back().push_back(Point2D(x,y));
            return true;
        }
        return false;
    }
    
    bool on_mouse_wheel(GLWindow &win, int state, int x, int y, int dx, int dy)
    {
        cout << "Wheel " << dy << endl;
        return true;
    }

    bool on_text(GLWindow &win, const char text[], int len)
    {
        for (int i=0; i<len; ++i) {
            char c = text[i];
            if (c == '\r') {
                c = '\n';
            } else if (c == (char)8) {
                if (!all_text.empty())
                    all_text = all_text.substr(0, all_text.length()-1);
                continue;
            }
                
            all_text += c;
        }
        return true;
    }

    bool on_key_down(GLWindow &win, int key)
    {
        double dx=0, dy=0;
        switch (key) {
        case KeyCode::LEFT: dx = -10; break;
        case KeyCode::RIGHT: dx = 10; break;
        case KeyCode::UP: dy = -10; break;
        case KeyCode::DOWN: dy = 10; break;
        default: return false;
        }
        for (size_t i=0; i<strokes.size(); ++i) {
            for (size_t j=0; j<strokes[i].size(); ++j) {
                strokes[i][j].x += dx;
                strokes[i][j].y += dy;
            }
        }
        return true;
    }    
};

void draw_line(const std::vector<Point2D> &points)
{
    glBegin(GL_LINE_STRIP);
    for (size_t i=0; i<points.size(); ++i) {
        glVertex2d(points[i].x, points[i].y);
    }        
    glEnd();
}

void pixel_ortho(const GLWindow& win)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, win.width(), win.height(), 0, -1, 1);
    glViewport(0, 0, win.width(), win.height());
}

int main()
{
    GLWindow win(400, 400, "test");
    GLWindow win2(400, 400, "test2");

    win.set_position(100, 100);
    win2.set_position(600, 100);

    Handler handler1, handler2;
    win.add_handler(&handler1);
    win2.add_handler(&handler2);

    win2.set_size(300, 500);
    win2.set_title("foo");
    
    while (win.alive() || win2.alive()) {
        if (win.alive())
        {
            GLWindow::ScopedContext ctx(win);
            pixel_ortho(win);
            glClearColor(1,0,0,0);
            glClear(GL_COLOR_BUFFER_BIT);
            for (size_t i=0; i<handler1.strokes.size(); ++i) {
                draw_line(handler1.strokes[i]);
            }
            glColor3ub(0,255,0);
            win.draw_text(10, 10, handler1.all_text.c_str());
            win.swap_buffers();
        }

        if (win2.alive())
        {
            GLWindow::ScopedContext ctx(win2);            
            pixel_ortho(win2);
            glClear(GL_COLOR_BUFFER_BIT);
            glClearColor(0,1,0,0);
            
            glColor3f(1.f, 0.f, 0.f);
            for (size_t i=0; i<handler2.strokes.size(); ++i) {
                draw_line(handler2.strokes[i]);
            }
            glColor3ub(128,255,128);
            win2.draw_text(10, 10, handler2.all_text.c_str());
            win2.swap_buffers();
        }
        
        GLWindow::handle_all_events();
    }
    return 0;
}
