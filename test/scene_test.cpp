#include <glwindow/scenewindow.hpp>
#include <iostream>
#include <string>
#include <GL/gl.h>

#include <deque>
#include <cstdlib>

using namespace glwindow;
using namespace std;

double rand_uniform()
{
    static const double f = 1.0 / RAND_MAX;
    return rand() * f;
}

struct Point3D
{
    double x[3];
    double &operator[](int i) { return x[i]; }
    double operator[](int i) const { return x[i]; }
};

Point3D pos = {{0,0,0}};
double vel[3] = {0,0,0};

Point3D follow = pos;

std::deque<Point3D> traj;
std::deque<Point3D> traj_follow;

void draw_scene()
{
    glLineWidth(2);
    
    glBegin(GL_LINES);

    glColor3ub(255,0,0);
    glVertex3f(-1.f, 0.f, 0.f);
    glVertex3f(1.f, 0.f, 0.f);

    glColor3ub(0,255,0);
    glVertex3f(0.f, -1.f, 0.f);
    glVertex3f(0.f, 1.f, 0.f);
    
    glColor3ub(0,0,255);
    glVertex3f(0.f, 0.f, -1.f);
    glVertex3f(0.f, 0.f, 1.f);
    
    glEnd();


    for (int i=0; i<3; ++i) {
        pos[i] += vel[i] * 0.01;
        vel[i] += -pos[i] * 0.0004 + (rand_uniform()*2.0 - 1.0)*0.01;
        vel[i] *= 0.9995;

        follow[i] += 0.003 * (pos[i] - follow[i]);
    }
        
    traj.push_back(pos);
    if (traj.size() > 1000)
        traj.pop_front();

    traj_follow.push_back(follow);
    if (traj_follow.size() > 400)
        traj_follow.pop_front();

    glDisable(GL_LINE_SMOOTH);
    {
        double inv_n = 1.0 / traj.size();
        glBegin(GL_LINE_STRIP);
        for (size_t i=0; i<traj.size(); ++i) {
            double t = i*inv_n;
            glColor4d(1,t,0,t);        
            glVertex3dv(traj[i].x);
        }
        glEnd();
    }

    {
        double inv_n = 1.0 / traj_follow.size();
        glBegin(GL_LINE_STRIP);
        for (size_t i=0; i<traj_follow.size(); ++i) {
            double t = i*inv_n;
            glColor4d(0,t,1,t);        
            glVertex3dv(traj_follow[i].x);
        }
        glEnd();
    }
}

int main()
{
    SceneWindow scene(400, 400, "test");

    while (scene.win.alive()) {
        if (scene.start_draw()) {
            draw_scene();
            scene.finish_draw();
        }
    }
    return 0;
}
