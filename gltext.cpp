#include "gltext.hpp"
#include <GL/gl.h>
#include <vector>
#include "font.inc"
#include <algorithm>

using namespace glwindow;

static unsigned char *font_data = 0;
static int font_refcount = 0;

static void init_font()
{
    ++font_refcount;
    if (font_data)
        return;
    
    int w = font_img_width;
    int h = font_img_height;
    font_data = new unsigned char[w*h*2];
    
    unpack_rle(font_luminance_rle, font_alpha_rle, w*h, font_data);
    // for (int i=0; i<w*h*2; ++i) {
    //     int x = font_data[i];
    //     font_data[i] = x > 64 ? 255 : 0;
    // }
}

static void release_font()
{
    if (--font_refcount == 0) {
        delete[] font_data;
        font_data = 0;
    }
}

static void make_font_texture(GLuint &tex_id)
{
    init_font();

    const int w = font_img_width;
    const int h = font_img_height;
    
    glPushAttrib(GL_ENABLE_BIT | GL_TEXTURE_BIT);
    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &tex_id);
    glBindTexture(GL_TEXTURE_2D, tex_id);
    ::glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    ::glPixelStorei(GL_UNPACK_ROW_LENGTH, w);
    ::glTexImage2D(GL_TEXTURE_2D, 0,
                   GL_LUMINANCE_ALPHA,
                   w, h, 0,
                   GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE,
                   font_data);
    ::glPixelStorei(GL_UNPACK_ALIGNMENT, 0);

    glPopAttrib();
}

struct GLText::State
{
    GLuint tex_id;
};

GLText::GLText()
{
}

GLText::~GLText()
{
    if (state) {
        glDeleteTextures(1, &state->tex_id);
        state.reset();
        release_font();
    }
}

bool GLText::init()
{
    if (state)
        return false;
    state = std::make_unique<State>();
    make_font_texture(state->tex_id);
    return true;
}

void GLText::compute_bounds(const char *text, int xywh[4])
{
    int min_y = 1000000;
    int max_y = -1000000;
    int min_x = 0;
    int x = 0, y = 0;
    int max_x = 0;
    while (*text) {
        int code = (unsigned char)*text++;
        if (code == '\n') {
            x = 0;
            y += font_size;
            continue;            
        }
        if (code == '\r') {
            x = 0;
            continue;
        }
        if (code == '\t') {
            x += 4 * glyphs[' '].advance;
            continue;
        }
        if (glyphs[code].code == -1)
            code = '?';
        const Glyph &g = glyphs[code];
        if (g.w > 0) {
            int y0 = y - g.oy + font_ascent - g.h;
            
            if (y0 < min_y)
                min_y = y0;
            if (y0 + g.h > max_y)
                max_y = y0 + g.h;
        }
        min_x = std::min(min_x, x + g.ox);
        max_x = std::max(max_x, x + g.ox + g.w);
        x += g.advance;
    }
    xywh[0] = 0;
    xywh[1] = min_y;
    xywh[2] = max_x;
    xywh[3] = max_y - min_y;
}

void GLText::draw(double x, double y, const char *text, int xywh[4])
{
    if (!state)
        init();
    
    glPushAttrib(GL_TRANSFORM_BIT | GL_COLOR_BUFFER_BIT | GL_ENABLE_BIT);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, state->tex_id);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    const double xstart = x;
    double max_x = x;

    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    glLoadIdentity();
    glScalef(1.f / (float)font_img_width, 1.f / (float)font_img_height, 1.f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    double min_y = 1e100;
    double max_y = -1e100;
    glBegin(GL_QUADS);
    while (*text) {
        int code = (unsigned char)*text++;
        if (code == '\n') {
            x = xstart;
            y += font_size;
            continue;            
        }
        if (code == '\r') {
            x = xstart;
            continue;
        }
        if (code == '\t') {
            x += 4 * glyphs[' '].advance;
            continue;
        }
        
        if (glyphs[code].code == -1)
            code = '?';
        const Glyph &g = glyphs[code];
        if (g.w > 0) {
            double x0 = x + g.ox;
            double y0 = y - g.oy + font_ascent - g.h;
            if (y0 < min_y)
                min_y = y0;
            if (y0 + g.h > max_y)
                max_y = y0 + g.h;
            
            glTexCoord2i(g.x, g.y);
            glVertex2d(x0, y0);
            
            glTexCoord2i(g.x + g.w, g.y);
            glVertex2d(x0 + g.w, y0);

            glTexCoord2i(g.x + g.w, g.y + g.h);
            glVertex2d(x0 + g.w, y0 + g.h);

            glTexCoord2i(g.x, g.y + g.h);
            glVertex2d(x0, y0 + g.h);
        }
        max_x = std::max(max_x, x + g.ox + g.w);
        x += g.advance;
    }
    glEnd();
    glPopMatrix();
    glPopAttrib();

    if (xywh) {
        xywh[0] = xstart;
        xywh[1] = min_y;
        xywh[2] = max_x - xstart;
        xywh[3] = max_y - min_y;
    }
}
