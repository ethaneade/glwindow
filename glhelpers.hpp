#pragma once

#include <GL/gl.h>

namespace glhelpers {

    struct GLImage8
    {
        const unsigned char * const data;
        const int width, height;
        const int stride;

        GLImage8() : data(0), width(0), height(0), stride(0) { }
        
        GLImage8(const unsigned char *d, int w, int h)
            : data(d), width(w), height(h), stride(w) { }

        GLImage8(const unsigned char *d, int w, int h, int s)
            : data(d), width(w), height(h), stride(s) { }        
    };

    
    void glDrawPixels(const GLImage8 &im);

    static inline
    void glDrawPixels(const unsigned char *d, int w, int h)
    {
        glDrawPixels(GLImage8(d, w, h));
    }
        
    void glTexImage2D(const GLImage8 &im, double sxy[2] = 0);

    static inline
    void glTexImage2D(const unsigned char *d, int w, int h)
    {
        glTexImage2D(GLImage8(d, w, h));
    }
    
    class GLTexture
    {        
    private:        
        GLuint id;
        GLTexture(const GLTexture& other);
        double sxy[2];
    public:        
        GLTexture(const GLImage8 &im, GLuint filter = GL_LINEAR);
        ~GLTexture();
        void scale_for_unit_image() const;
       
        static void set_env_mode(GLint mode);
        static void set_filter(GLint filter);
    };
               
    void glTexturedBox(double x, double y, double w, double h);

    void glTexturedBox(const GLImage8 &im,
                       double x, double y, double w, double h,
                       GLuint filter = GL_LINEAR);
    
    void glOrthoBox(double w, double h);

    static inline
    void glOrthoPixels(double w, double h)
    {
        glViewport(0,0,w,h);
        glOrthoBox(w,h);
    }
    

    struct GLPushMatrix
    {
        GLPushMatrix() { glPushMatrix(); }
        ~GLPushMatrix() { glPopMatrix(); }
    };
    
    template <class V>
    void glVertex2(const V& v)
    {
        glVertex2d(v[0], v[1]);
    }

    template <class V>
    void glVertex3(const V& v)
    {
        glVertex3d(v[0], v[1], v[2]);
    }
    
    static inline
    void glLine(double x0, double y0, double x1, double y1)
    {
        glBegin(GL_LINES);
        glVertex2d(x0,y0);
        glVertex2d(x1,y1);
        glEnd();            
    }

    template <class V1, class V2>
    void glLine(const V1 &a, const V2 &b)
    {
        glLine(a[0], a[1], b[0], b[1]);
    }
    
    static inline
    void glCross(double x, double y, double radius)
    {
        glBegin(GL_LINES);
        glVertex2d(x-radius, y);
        glVertex2d(x+radius, y);
        glVertex2d(x, y-radius);
        glVertex2d(x, y+radius);
        glEnd();
    }
    
    template <class V>
    void glCross(const V& center, double radius)
    {
        glCross(center[0], center[1], radius);
    }

    inline
    void glBox(double x, double y, double w, double h)
    {
        glBegin(GL_QUADS);
        glVertex2d(x,y);
        glVertex2d(x+w, y);
        glVertex2d(x+w, y+h);
        glVertex2d(x, y+h);
        glEnd();
    }
        
    void glUnitCircle(int segs=20);

    static inline
    void glCircle(double x, double y, double r, int segs=20)
    {
        GLPushMatrix pm;
        glTranslated(x,y,0);
        glScaled(r,r,1);
        glUnitCircle(segs);
    }

}
