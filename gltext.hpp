#pragma once

#include <memory>

namespace glwindow {

    class GLText
    {
    public:
        GLText();
        ~GLText();

        bool init();
        void compute_bounds(const char *text, int xywh[4]);
        void draw(double x, double y, const char *text, int xywh[4]=0);
    private:
        struct State;
        std::unique_ptr<State> state;
    };
}
