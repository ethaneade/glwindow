#include <cmath>
#include <GL/gl.h>
#include "glhelpers.hpp"

using namespace glhelpers;

void glhelpers::glDrawPixels(const GLImage8 &im)
{
    ::glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    ::glPixelStorei(GL_UNPACK_ROW_LENGTH, im.stride);
    ::glDrawPixels(im.width, im.height, GL_LUMINANCE, GL_UNSIGNED_BYTE, im.data);
    ::glPixelStorei(GL_UNPACK_ALIGNMENT, 0);
}

static int power_of_two_at_least(int x)
{
    if (x == 0)
        return 0;
    int y = 1;
    while (y < x)
        y <<= 1;
    return y;
}

void glhelpers::glTexImage2D(const GLImage8 &im, double sxy[2])
{
    int wpot = power_of_two_at_least(im.width);
    int hpot = power_of_two_at_least(im.height);
    
    ::glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    ::glPixelStorei(GL_UNPACK_ROW_LENGTH, im.stride);
    ::glTexImage2D(GL_TEXTURE_2D, 0,
                   GL_LUMINANCE,
                   wpot, hpot, 0,
                   GL_LUMINANCE, GL_UNSIGNED_BYTE,
                   0);
    
    ::glTexSubImage2D(GL_TEXTURE_2D, 0,
                      0, 0,
                      im.width, im.height,
                      GL_LUMINANCE, GL_UNSIGNED_BYTE,
                      im.data);
                      
    ::glPixelStorei(GL_UNPACK_ALIGNMENT, 0);

    if (sxy) {
        sxy[0] = im.width/(double)wpot;
        sxy[1] = im.height/(double)hpot;
    }
}


void glhelpers::GLTexture::scale_for_unit_image() const
{
    GLint mode;
    glGetIntegerv(GL_MATRIX_MODE, &mode);
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glScaled(sxy[0], sxy[1], 1.0);
    glMatrixMode(mode);        
}

glhelpers::GLTexture::GLTexture(const GLImage8 &im, GLuint filter)
{
    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexImage2D(im, sxy);

    scale_for_unit_image();
    GLTexture::set_filter(filter);
}

glhelpers::GLTexture::~GLTexture()
{
    glDeleteTextures(1, &id);
}

void glhelpers::GLTexture::set_env_mode(GLint mode)
{
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, mode);
}
        
void glhelpers::GLTexture::set_filter(GLint filter)
{
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
}
               
void glhelpers::glTexturedBox(double x, double y, double w, double h)
{
    glBegin(GL_QUADS);
    glTexCoord2f(0,0);
    glVertex2d(x,y);
    glTexCoord2f(1,0);
    glVertex2d(x+w, y);
    glTexCoord2f(1,1);
    glVertex2d(x+w, y+h);
    glTexCoord2f(0,1);
    glVertex2d(x, y+h);
    glEnd();
}

void glhelpers::glTexturedBox(const GLImage8 &im,
                              double x, double y, double w, double h,
                              GLuint filter)
{
    GLTexture tex(im, filter);
    tex.scale_for_unit_image();
    glTexturedBox(x, y, w, h);
}

void glhelpers::glOrthoBox(double w, double h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();  
    glOrtho(0, w, h, 0, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPixelZoom(1,-1);
    glRasterPos2i(0,0);    
}        
        
void glhelpers::glUnitCircle(int segs)
{
    double dtheta = 2 * M_PI / segs;
    double c = cos(dtheta);
    double s = sin(dtheta);
    double x = 1;
    double y = 0;

    glBegin(GL_LINE_STRIP);
    glVertex2d(1,0);        
    for (int i=1; i<segs; ++i) {
        double x1 = x*c - y*s;
        y = x*s + y*c;
        x = x1;
        glVertex2d(x,y);
    }
    glVertex2d(1,0);
    glEnd();
}
