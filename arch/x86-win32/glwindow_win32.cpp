#include "glwindow.hpp"
using namespace glwindow;

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <windows.h>

#include <GL/gl.h>
#include <stdio.h>

struct GLWindow::SystemState
{
    HWND hwnd;
    HGLRC hrc;
    int width, height;
    bool visible;
    std::shared_ptr<SharedState> shared_state;

    SystemState() {
        width = height = 0;
        hwnd = 0;
        hrc = 0;
        visible = false;
        shared_state = GLWindow::shared_state;
    }

    static LONG WINAPI WinProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
    static HWND CreateOpenGLWindow(const char* title,
                                   int x, int y, int width, int height,
                                   GLWindow *glwin);
};

static int convert_button_state(WPARAM state)
{
    int s = 0;
    if (state & MK_LBUTTON) s |= ButtonEvent::LEFT;
    if (state & MK_MBUTTON) s |= ButtonEvent::MIDDLE;
    if (state & MK_RBUTTON) s |= ButtonEvent::RIGHT;
    if (state & MK_CONTROL) s |= ButtonEvent::MODKEY_CTRL;
    if (state & MK_SHIFT) s |= ButtonEvent::MODKEY_SHIFT;
    return s;
}

static int msg_to_button(UINT msg)
{
    switch (msg) {
    case WM_LBUTTONDOWN:
    case WM_LBUTTONUP:
        return ButtonEvent::LEFT;
    case WM_MBUTTONDOWN:
    case WM_MBUTTONUP:
        return ButtonEvent::MIDDLE;
    case WM_RBUTTONDOWN:
    case WM_RBUTTONUP:
        return ButtonEvent::RIGHT;
    default:
        return 0;
    }
}

static int convert_keycode(int key)
{
    switch (key) {
    case VK_BACK: return KeyCode::BACKSPACE;
    case VK_TAB: return KeyCode::TAB;
    case VK_RETURN: return KeyCode::ENTER;
    case VK_SHIFT: return KeyCode::SHIFT;
    case VK_LSHIFT: return KeyCode::SHIFT;
    case VK_RSHIFT: return KeyCode::SHIFT;
    case VK_CONTROL: return KeyCode::CTRL;
    case VK_LCONTROL: return KeyCode::CTRL;
    case VK_RCONTROL: return KeyCode::CTRL;
    case VK_MENU: return KeyCode::ALT;
    case VK_LMENU: return KeyCode::ALT;
    case VK_RMENU: return KeyCode::ALT;
    case VK_LWIN: return KeyCode::SUPER;
    case VK_RWIN: return KeyCode::SUPER;
    case VK_CAPITAL: return KeyCode::CAPSLOCK;
    case VK_DELETE: return KeyCode::DEL;
    case VK_ESCAPE: return KeyCode::ESCAPE;
    case VK_LEFT: return KeyCode::LEFT;
    case VK_UP: return KeyCode::UP;
    case VK_RIGHT: return KeyCode::RIGHT;
    case VK_DOWN: return KeyCode::DOWN;
    }
    return key;
}

LONG WINAPI
GLWindow::SystemState::WinProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (msg == WM_NCCREATE) {
        CREATESTRUCT &cs = *(CREATESTRUCT*)lParam;
        GLWindow *win = (GLWindow*)cs.lpCreateParams;
        SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)win);
        return 1;
    }
    
    GLWindow *win = (GLWindow*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
    if (win == 0) {
        return DefWindowProc(hWnd, msg, wParam, lParam);
    }

    EventDispatcher dispatcher(win->handlers);
    char text[10];
    
    switch(msg) {
    case WM_CREATE:
        return 0;
        
    // case WM_PAINT:
    //     return 0;
        
    case WM_KEYDOWN:
        if (dispatcher.on_key_down(*win, convert_keycode(wParam)))
            return 0;
        break;

    case WM_KEYUP:
        if (dispatcher.on_key_up(*win, convert_keycode(wParam)))
            return 0;
        break;
       
    case WM_MOUSEMOVE: {
        int x = (signed short)(lParam & 0xFFFF);
        int y = (signed short)(lParam >> 16);
        int state = convert_button_state(wParam);
        if (dispatcher.on_mouse_move(*win, state, x, y))
            return 0;
        break;
    }
        
    case WM_LBUTTONDOWN:
    case WM_MBUTTONDOWN:
    case WM_RBUTTONDOWN: {
        int x = (signed short)(lParam & 0xFFFF);
        int y = (signed short)(lParam >> 16);
        int btn = msg_to_button(msg);
        int state = convert_button_state(wParam);
        if (dispatcher.on_button_down(*win, btn, state, x, y))
            return 0;
        break;
    }

    case WM_LBUTTONUP:
    case WM_MBUTTONUP:
    case WM_RBUTTONUP: {
        int x = (signed short)(lParam & 0xFFFF);
        int y = (signed short)(lParam >> 16);
        int btn = msg_to_button(msg);
        int state = convert_button_state(wParam);
        if (dispatcher.on_button_up(*win, btn, state, x, y))
            return 0;
        break;
    }

    case WM_MOUSEWHEEL: {
        int x = (signed short)(lParam & 0xFFFF);
        int y = (signed short)(lParam >> 16);
        int dy = GET_WHEEL_DELTA_WPARAM(wParam);
        int state = convert_button_state(wParam);
        if (dispatcher.on_mouse_wheel(*win, state, x, y, 0, dy))
            return 0;
        break;
    }
        
    case WM_CHAR:
        text[0] = (char)wParam;
        text[1] = 0;
        dispatcher.on_text(*win, text, 1);
        return 0;

    case WM_WINDOWPOSCHANGED: {
        WINDOWPOS *wp = (WINDOWPOS*)lParam;
        RECT r;
        GetClientRect(hWnd, &r);
        int w = r.right - r.left;
        int h = r.bottom - r.top;
        win->sys_state->width = w;
        win->sys_state->height = h;
        win->sys_state->visible = 0 != IsWindowVisible(hWnd);
        dispatcher.on_resize(*win, wp->x, wp->y, w, h);
        return 0;
    }
        
    case WM_CLOSE:
        if (!dispatcher.on_close(*win))
            win->destroy();
        return 0;
        
    case WM_DESTROY:
        return 0;
    }

    return DefWindowProc(hWnd, msg, wParam, lParam);
}

static HINSTANCE hInstance = 0;

HWND
GLWindow::SystemState::CreateOpenGLWindow(const char* title,
                                          int x, int y, int width, int height,
                                          GLWindow *glwin)
{    
    /* only register the window class once - use hInstance as a flag. */
    if (!hInstance) {
        hInstance = GetModuleHandle(NULL);
        WNDCLASS    wc = {0};
        wc.style         = CS_OWNDC;
        wc.lpfnWndProc   = (WNDPROC)GLWindow::SystemState::WinProc;
        wc.hInstance     = hInstance;
        wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO);
        wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
        wc.lpszClassName = "GLWindow";
             
        if (!RegisterClass(&wc))
            return 0;
    }

    RECT r;
    r.left = 0;
    r.top = 0;
    r.right = width;
    r.bottom = height;

    // Accommodate non-client area
    AdjustWindowRect(&r, WS_OVERLAPPEDWINDOW, 0);
             
    HWND hWnd = CreateWindow("GLWindow", title,
                             WS_OVERLAPPEDWINDOW,
                             x, y, r.right, r.bottom,
                             NULL, NULL,
                             hInstance,
                             glwin);
    
    if (hWnd == 0)
        return 0;

    {
        PIXELFORMATDESCRIPTOR pfd = {0};
        pfd.nSize        = sizeof(pfd);
        pfd.nVersion     = 1;
        pfd.dwFlags      = (PFD_DRAW_TO_WINDOW |
                            PFD_SUPPORT_OPENGL |
                            PFD_DOUBLEBUFFER);
        pfd.iPixelType   = PFD_TYPE_RGBA;
        pfd.cColorBits   = 32;
        
        HDC hDC = GetDC(hWnd);
        int pf = ChoosePixelFormat(hDC, &pfd);
        if (pf == 0) {
            ReleaseDC(hWnd, hDC);
            DestroyWindow(hWnd);
            return 0;
        }
        if (SetPixelFormat(hDC, pf, &pfd) == FALSE) {
            ReleaseDC(hWnd, hDC);
            DestroyWindow(hWnd);
            return 0;
        }
        
        DescribePixelFormat(hDC, pf, sizeof(pfd), &pfd);
        ReleaseDC(hWnd, hDC);
    }             
    
    return hWnd;
}    
             
GLWindow::GLWindow(int w, int h, const char *title)
        : sys_state(std::make_unique<SystemState>())
{
    prev_active = 0;
    sys_state->hwnd = SystemState::CreateOpenGLWindow(title,
                                                      CW_USEDEFAULT, CW_USEDEFAULT,
                                                      w, h,
                                                      this);
    if (sys_state->hwnd == 0)
        return;

    sys_state->width = w;
    sys_state->height = h;
             
    HDC hdc = GetDC(sys_state->hwnd);
    sys_state->hrc = wglCreateContext(hdc);
    ReleaseDC(sys_state->hwnd, hdc);
    if (sys_state->hrc == 0) {
        DestroyWindow(sys_state->hwnd);
        sys_state->hwnd = 0;
        return;
    }
    ShowWindow(sys_state->hwnd, SW_SHOW);
    set_title(title);
    sys_state->visible = true;

    push_context();
    text.init();
    pop_context();
    
    GLWindow::add_window(this);
}

GLWindow::~GLWindow()
{
    destroy();
}

int GLWindow::width() const
{
    return sys_state->width;
}

int GLWindow::height() const
{
    return sys_state->height;
}

bool GLWindow::visible() const
{
    return sys_state->visible;
}

bool GLWindow::alive() const
{
    return sys_state->hwnd != 0;
}
        
bool GLWindow::make_current()
{
    if (!sys_state->hwnd)
        return false;
    
    HDC hdc = GetDC(sys_state->hwnd);
    wglMakeCurrent(hdc, sys_state->hrc);
    ReleaseDC(sys_state->hwnd, hdc);

    set_active_context(this);
    return true;
}

void GLWindow::swap_buffers()
{
    HDC hdc = GetDC(sys_state->hwnd);
    SwapBuffers(hdc);
    ReleaseDC(sys_state->hwnd, hdc);
}
        
void GLWindow::set_size(int w, int h)
{
    if (!sys_state->hwnd)
        return;
    SetWindowPos(sys_state->hwnd, 0, 0, 0, w, h, SWP_NOZORDER | SWP_NOMOVE);
}

void GLWindow::set_position(int x, int y)
{
    if (!sys_state->hwnd)
        return;
    SetWindowPos(sys_state->hwnd, 0, x, y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
}
        
void GLWindow::set_title(const char* title)
{
    if (!sys_state->hwnd)
        return;
    SetWindowText(sys_state->hwnd, title);
}
        
void GLWindow::handle_events()
{
    if (!sys_state->hwnd)
        return;
    
    MSG   msg;	/* message */             
    while(PeekMessage(&msg, sys_state->hwnd, 0, 0, PM_REMOVE)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
}
        
void GLWindow::destroy()
{
    if (!sys_state->hwnd)
        return;

    GLWindow::remove_window(this);
    if (get_active_context() == this) {
        wglMakeCurrent(NULL, NULL);
        set_active_context(nullptr);
    }
    
    if (sys_state->hrc) {
        wglDeleteContext(sys_state->hrc);
        sys_state->hrc = 0;
    }
    
    if (sys_state->hwnd) {
        DestroyWindow(sys_state->hwnd);
        sys_state->hwnd = 0;        
    }
}
