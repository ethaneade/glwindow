#include "glwindow.hpp"
#include <vector>
#include <cstddef>

using namespace glwindow;

struct GLWindow::SharedState {
    std::vector<GLWindow*> all_windows;
    GLWindow *active_context = nullptr;
};
std::shared_ptr<GLWindow::SharedState> GLWindow::shared_state = std::make_shared<SharedState>();

void GLWindow::add_window(GLWindow *win)
{
    shared_state->all_windows.push_back(win);
}

bool GLWindow::remove_window(GLWindow *win)
{
    auto &all_windows = shared_state->all_windows;
    for (size_t i=0; i<all_windows.size(); ++i) {
        if (all_windows[i] == win) {
            all_windows[i] = all_windows.back();
            all_windows.pop_back();
            return true;
        }
    }
    return false;
}

void GLWindow::handle_all_events()
{
    auto &all_windows = shared_state->all_windows;
    for (size_t i=0; i<all_windows.size(); ++i)
        all_windows[i]->handle_events();
}

void GLWindow::set_active_context(GLWindow *win) {
    shared_state->active_context = win;
}

GLWindow *GLWindow::get_active_context() {
    return shared_state->active_context;
}

bool GLWindow::push_context()
{
    prev_active = shared_state->active_context;
    return make_current();
}

void GLWindow::pop_context()
{
    if (shared_state->active_context != this)
        return;

    if (prev_active) {
        prev_active->make_current();
    } else {
        shared_state->active_context = 0;
    }
}

void GLWindow::add_handler(EventHandler* handler)
{
    handlers.push_back(handler);
}

bool GLWindow::remove_handler(EventHandler* handler)
{
    std::vector<EventHandler*>::reverse_iterator it;
    for (it = handlers.rbegin(); it != handlers.rend(); ++it) {
        if (*it == handler) {
            handlers.erase(it.base());
            return true;
        }
    }
    return false;
}


void GLWindow::draw_text(double x, double y, const char *s, int xywh[4])
{
    text.draw(x,y,s, xywh);
}

bool EventDispatcher::on_key_down(GLWindow& win, int key) {
    for (int i=handlers.size()-1; i>=0; --i)
        if (handlers[i]->on_key_down(win, key))
            return true;
    return false;
}
bool EventDispatcher::on_key_up(GLWindow& win, int key) {
    for (int i=handlers.size()-1; i>=0; --i)
        if (handlers[i]->on_key_up(win, key))
            return true;
    return false;
}
bool EventDispatcher::on_text(GLWindow& win, const char *text, int len) {
    for (int i=handlers.size()-1; i>=0; --i)
        if (handlers[i]->on_text(win, text, len))
            return true;
    return false;
}

bool EventDispatcher::on_button_down(GLWindow& win, int btn, int state, int x, int y) {
    for (int i=handlers.size()-1; i>=0; --i)
        if (handlers[i]->on_button_down(win, btn, state, x, y))
            return true;
    return false;
}

bool EventDispatcher::on_button_up(GLWindow& win, int btn, int state, int x, int y) {
    for (int i=handlers.size()-1; i>=0; --i)
        if (handlers[i]->on_button_up(win, btn, state, x, y))
            return true;
    return false;
}

bool EventDispatcher::on_mouse_move(GLWindow& win, int state, int x, int y) {
    for (int i=handlers.size()-1; i>=0; --i)
        if (handlers[i]->on_mouse_move(win, state, x, y))
            return true;
    return false;
}

bool EventDispatcher::on_mouse_wheel(GLWindow& win, int state, int x, int y, int dx, int dy) {
    for (int i=handlers.size()-1; i>=0; --i)
        if (handlers[i]->on_mouse_wheel(win, state, x, y, dx, dy))
            return true;
    return false;
}

bool EventDispatcher::on_resize(GLWindow &win, int x, int y, int w, int h) {
    for (int i=handlers.size()-1; i>=0; --i)
        if (handlers[i]->on_resize(win, x, y, w, h))
            return true;
    return false;
}

bool EventDispatcher::on_close(GLWindow &win) {
    for (int i=handlers.size()-1; i>=0; --i)
        if (handlers[i]->on_close(win))
            return true;
    return false;
}
