all:
	+ make -C arch/x86-linux
	+ make -C arch/x86-win32

clean:
	+ make -C arch/x86-linux clean
	+ make -C arch/x86-win32 clean

depend:
	+ make -C arch/x86-linux depend
	+ make -C arch/x86-win32 depend
